# frozen_string_literal: true

module SemverDialects
  VERSION = '3.4.0'
end
